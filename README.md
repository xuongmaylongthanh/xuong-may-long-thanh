# Xưởng may Long Thanh

Xưởng may Long Thanh - Xưởng chuyên may quần áo trẻ em, chuyên bỏ sỉ quần áo trẻ em đi các tỉnh thành trong cả nước, các sản phẩm được các nhà thiết kế thời trang giàu kinh nghiệm vẽ nên những mẫu thời trang đẹp, với chất lượng an toàn và giá thành phù hợp với mọi gia đình.

Liên hệ Xưởng may Long Thanh 

Địa chỉ: 19 Phú Hòa, Phường 7, Quận Tân Bình, TP Hồ Chí Minh 

Hotline: 0908 620 739 (Viber/Line/Sky/Whatsap) 

Zalo: 01654001251 - 0938930470 - 0912685470 

Website: http://xuongmaylongthanh.vn